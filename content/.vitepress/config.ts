import { defineConfig } from "vitepress";

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "Minoko Novel",
  description: "Reality Mixed With Persona",
  markdown: {
    theme: "dracula",
  },
  base: "/minoko-novel/",
  vue: {
    template: {
      compilerOptions: {
        isCustomElement: (tag) => tag === "iconify-icon",
      },
    },
  },
  outDir: "../public",
  head: [
    [
      "link",
      {
        rel: "apple-touch-icon",
        sizes: "180x180",
        href: "/minoko-novel/apple-touch-icon.png",
      },
    ],
    [
      "link",
      { rel: "icon", sizes: "32x32", href: "/minoko-novel/favicon-32x32.png" },
    ],
    [
      "link",
      { rel: "icon", sizes: "16x16", href: "/minoko-novel/favicon-16x16.png" },
    ],
    ["link", { rel: "manifest", href: "/minoko-novel/site.webmanifest" }],
    [
      "link",
      {
        rel: "mask-icon",
        href: "/minoko-novel/safari-pinned-tab.svg",
        color: "#5bbad5",
      },
    ],
    ["link", { rel: "shortcut icon", href: "/minoko-novel/favicon.ico" }],
    ["meta", { name: "msapplication-TileColor", content: "#da532c" }],
    ["meta", { name: "theme-color", content: "#9d174d" }],
  ],
  transformHead: ({ assets }) => {
    // adjust the regex accordingly to match your font
    const files = assets.filter(file => file.match(/(jost|space-mono).*\.woff2$/))
    return files.map((file) => [
      'link',
      {
        rel: 'preload',
        href: file,
        as: 'font',
        type: 'font/woff2',
        crossorigin: ''
      }
    ])
  },
});
