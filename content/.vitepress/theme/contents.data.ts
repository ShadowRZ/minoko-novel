import type { ContentData } from "vitepress";

declare const data: ContentData[]
export { data }

import { createContentLoader } from "vitepress";
export default createContentLoader("main/*.md");
