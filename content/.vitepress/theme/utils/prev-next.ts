import { computed } from "vue";
import { data as posts } from "../contents.data";

import { useRoute, withBase } from "vitepress";

export function usePrevNext() {
  return computed(() => {
    const { path } = useRoute();

    function getIndex() {
      const index = posts.findIndex((i) => {
        return withBase(i.url) === path;
      });

      return index;
    }

    return {
      prev: posts[getIndex() - 1]?.url,
      next: posts[getIndex() + 1]?.url,
    } as {
      prev?: string;
      next?: string;
    };
  });
}
