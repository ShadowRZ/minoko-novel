/** @type {import('tailwindcss').Config} */

const round = (num) =>
  num
    .toFixed(7)
    .replace(/(\.[0-9]+?)0+$/, "$1")
    .replace(/\.0$/, "");
const rem = (px) => `${round(px / 16)}rem`;
const em = (px, base) => `${round(px / base)}em`;

module.exports = {
  content: [
    "./content/**/*.{md,js,vue}",
    "./content/.vitepress/theme/**/*.{js,vue}",
  ],
  theme: {
    fontFamily: {
      sans: ["Jost", "sans-serif"],
      monospace: ["Space Mono", "monospace", "sans-serif"],
      toc: ["Space Mono", "sans-serif"],
    },
    extend: {
      typography: ({ theme }) => ({
        minoko: {
          css: {
            fontSize: rem(18),
            lineHeight: round(24 / 16),
            h1: {
              marginBottom: em(15, 30),
            },
            h2: {
              marginTop: em(20, 20),
            },
            hr: {
              marginTop: 0,
              marginBottom: "0.5em",
            },
            "--tw-prose-body": theme("colors.pink[800]"),
            "--tw-prose-headings": theme("colors.pink[800]"),
            "--tw-prose-lead": theme("colors.pink[700]"),
            "--tw-prose-links": theme("colors.pink[800]"),
            "--tw-prose-bold": theme("colors.pink[800]"),
            "--tw-prose-counters": theme("colors.pink[600]"),
            "--tw-prose-bullets": theme("colors.pink[400]"),
            "--tw-prose-hr": theme("colors.pink[300]"),
            "--tw-prose-quotes": theme("colors.pink[800]"),
            "--tw-prose-quote-borders": theme("colors.pink[300]"),
            "--tw-prose-captions": theme("colors.pink[700]"),
            "--tw-prose-code": theme("colors.pink[900]"),
            "--tw-prose-pre-code": theme("colors.pink[100]"),
            "--tw-prose-pre-bg": "#282A36",
            "--tw-prose-th-borders": theme("colors.pink[300]"),
            "--tw-prose-td-borders": theme("colors.pink[200]"),
            "--tw-prose-invert-body": theme("colors.pink[200]"),
            "--tw-prose-invert-headings": theme("colors.pink[200]"),
            "--tw-prose-invert-lead": theme("colors.pink[200]"),
            "--tw-prose-invert-links": theme("colors.pink[200]"),
            "--tw-prose-invert-bold": theme("colors.pink[200]"),
            "--tw-prose-invert-counters": theme("colors.pink[400]"),
            "--tw-prose-invert-bullets": theme("colors.pink[600]"),
            "--tw-prose-invert-hr": theme("colors.pink[700]"),
            "--tw-prose-invert-quotes": theme("colors.pink[100]"),
            "--tw-prose-invert-quote-borders": theme("colors.pink[700]"),
            "--tw-prose-invert-captions": theme("colors.pink[400]"),
            "--tw-prose-invert-code": theme("colors.pink[200]"),
            "--tw-prose-invert-pre-code": theme("colors.pink[300]"),
            "--tw-prose-invert-pre-bg": "#282A36",
            "--tw-prose-invert-th-borders": theme("colors.pink[600]"),
            "--tw-prose-invert-td-borders": theme("colors.pink[700]"),
          },
        },
        novel: {
          css: {
            p: {
              textIndent: "2em",
              marginBottom: "0.5em",
              marginTop: "0.5em",
            },
          },
        },
      }),
    },
  },
  plugins: [require("@tailwindcss/typography")],
};
